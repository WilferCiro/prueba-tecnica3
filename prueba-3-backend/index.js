// index.js
/*  EXPRESS */
var sqlite3 = require('sqlite3').verbose();
const express = require('express');
const axios = require('axios');
var cors = require('cors')
const jwt = require('jsonwebtoken');
const jwt2 = require('express-jwt');

var db = new sqlite3.Database('database.sqlite3');
const app = express();
app.set('view engine', 'ejs');
app.use(cors())

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
// GitHub
const clientID = 'a940848cdfc92d88577f'
const clientSecret = 'e05cc30cfd136623010c9bb322a2901998b85309'

const SECRET_KEY = "SECRET_KEY";

app.get('/', function(req, res) {
	res.json({ status: "Running" });
});



/// Login and singup

app.post('/cliente/add', jwt2({ secret: SECRET_KEY, algorithms: ['HS256'] }), function(req, res) {

	let email = req.body.email;
	let nombres = req.body.nombres;
	let apellidos = req.body.apellidos;
	let cedula = req.body.cedula;
	let sqlInsert = "INSERT INTO cliente (email, nombres, apellidos, cedula) VALUES (?,?,?,?)"
	let paramsInsert = [email, nombres, apellidos, cedula]
	db.run(sqlInsert, paramsInsert, function (err, row) {
		if (err){
			res.status(400).json({
				"error": "Error al registrar el usuario"
			})
		}
		res.json({
			"success": true
		})
	});
});

app.get('/clientes/get', jwt2({ secret: SECRET_KEY, algorithms: ['HS256'] }), function(req, res) {
	let page = req.query.page;
	if (page === null) {
		page = 1;
	}
	let sqlInsert = "select * from cliente limit 10"
	db.all(sqlInsert, [], function (err, rows) {
		if(err){
			res.status(400).json({
				"error": "Verifique sus datos"
			});
		}
		else{
			res.json({
				"success": true,
				"data" : rows
			});
		}
	});
});


app.put('/cliente/update/:id', jwt2({ secret: SECRET_KEY, algorithms: ['HS256'] }), function(req, res) {
	let id = req.params.id;
	let email = req.body.email;
	let nombres = req.body.nombres;
	let apellidos = req.body.apellidos;
	let cedula = req.body.cedula;
	let sqlInsert = "update cliente set email=?, nombres=?, apellidos=?, cedula=? where id=?"
	db.run(sqlInsert, [email, nombres, apellidos, cedula, id], function (err, rows) {
		if(err){
			res.status(400).json({
				"error": "Verifique sus datos"
			});
		}
		else{
			res.json({
				"success": true
			});
		}
	});
});

app.delete('/cliente/delete/:id', jwt2({ secret: SECRET_KEY, algorithms: ['HS256'] }), function(req, res) {
	let id = req.params.id;
	let sqlInsert = "delete from cliente where id=?"
	db.run(sqlInsert, [id], function (err, rows) {
		console.log(err);
		if(err){
			res.status(400).json({
				"error": "Verifique sus datos"
			});
		}
		else{
			res.json({
				"success": true
			});
		}
	});
});


app.post('/login', function(req, res) {
	let email = req.body.email;
	let password = req.body.password;
	let sql = "select * from user where email=? and password=?"
	let params = [email, password]
	db.get(sql, params, (err, rows) => {
		if (err) {
			res.status(400).json({
				"error": "Hubo un error al consultar datos, intente de nuevo"
			});
		}
		else{
			if(!rows){
				res.status(400).json({
					"error": "Verifique sus datos de inicio de sesión"
				});
			}
			else{
				const token = jwt.sign(
					{ admin: rows["admin"] },
						SECRET_KEY,
					{
						expiresIn: "24h",
					}
				);
				res.json({
					"success": true,
					"token" : token
				});
			}
		}
	});
});



const port = process.env.PORT || 2400;
app.listen(port , () => console.log('App listening on port ' + port));
