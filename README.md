#### Prueba técnica creada por Wilfer Daniel Ciro Maya

Para ejecutar el proyecto, debe correr dos partes, el front end y el backend

## Front end:

Ejecutar

```console
	yarn install
	yarn start
```

## Backend

Ejecutar

```console
	yarn install
	node index.js
```


## Autenticación:

Existen dos usuarios:

**Administrador:**

- Email: admin@admin.com
- password: admin

**Invitado:**

- Email: user@user.com
- password: user
