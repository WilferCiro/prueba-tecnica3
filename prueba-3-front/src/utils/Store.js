/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Singleton class.
*/

class Store {
	static instance = null;

	constructor(){
		if (Store.instance!==null){
			return Store.instance;
		}
		Store.instance = this;

		// Methods
		this.addData    = this.addData.bind(this);
		this.getData    = this.getData.bind(this);
		this.removeData = this.removeData.bind(this);

		// User methods
		this.logout       = this.logout.bind(this);

		this.storeID      = this.storeID.bind(this);
		this.getID        = this.getID.bind(this);

		// Repositories
		this.toggleFavorite = this.toggleFavorite.bind(this);
		this.isFavorite     = this.isFavorite.bind(this);
		this.getFavorites   = this.getFavorites.bind(this);
		this.removeFavorite = this.removeFavorite.bind(this);
	}
	removeData(key) {
		localStorage.removeItem(key);
	}

	addData(key, value) {
		localStorage.setItem(key, JSON.stringify(value));
	}

	getData(key) {
		let value = localStorage.getItem(key);
		try{
			return JSON.parse(value);
		}
		catch(e) {}
		return null;
	}

	storeID(id) {
		this.addData("id", id);
	}
	getID() {
		return this.getData("id") || null;
	}

	// User handler
	logout() {
		this.removeData("id");
	}

	// Repositories
	toggleFavorite(newData) {
		let data = this.getData("favorites") || [];
		let udted = false;
		for (let i in data) {
			if (data[i]["id"] === newData["id"]) {
				data.splice(i, 1);
				udted = true;
				break;
			}
		}

		if(!udted) {
			data.push(newData);
		}
		this.addData("favorites", data);
	}

	getFavorites(user) {
		let newData = [];
		let data = this.getData("favorites") || [];
		for (let i in data) {
			if (data[i]["user"] === user) {
				newData.push(data[i]);
			}
		}
		return newData;
	}

	removeFavorite(id) {
		let data = this.getData("favorites") || [];
		for (let i in data) {
			if (data[i]["id"] === id) {
				data.splice(i, 1);
			}
		}
		this.addData("favorites", data);
	}

	isFavorite(id) {
		let data = this.getData("favorites") || [];
		for (let i in data) {
			if (data[i]["id"] === id) {
				return true;
			}
		}
		return false;
	}


}

export default new Store();
