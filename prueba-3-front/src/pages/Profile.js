/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página para iniciar sesión
*/

// React
import {useState, useEffect, useContext} from 'react';
import { useHistory } from "react-router-dom";
import jwt      from 'jsonwebtoken';

// Own components
import Store            from '../utils/Store';
import Header           from '../components/Header';
import ClienteForm      from '../components/ClienteForm';
import {UserContext}    from '../context/UserContext';
// Ant components and icons
import {
	Button,
	message,
	Skeleton,
	Divider,
	Table,
	Modal,
	Tooltip
} from 'antd';
import {
	EditFilled,
	DeleteFilled
} from '@ant-design/icons';

const { confirm } = Modal;


function ProfilePage() {

	let history = useHistory();
	let openFormEdit = null;
	const [data, setData] = useState(null);
	const [admin, setAdmin] = useState(false);

	const userContext = useContext(UserContext);

	const getClientesData = async () => {
		//let token = Store.getID();
		let token = userContext.user;
		let goLogin = false;
		if (!token) {
			goLogin = true;
		}

		try {
			let options = {
				algorithms: ['HS256', 'HS384', 'HS512'],
				ignoreExpiration: false
			}
			if(token && token !== "undefined"){
				let decoded = jwt.verify(token, "SECRET_KEY", options);
				console.log(decoded)
				let newAadmin = decoded["admin"] === 1 || decoded["admin"] === "1" ? true : false;
				setAdmin(newAadmin);
			}
		}
		catch (e) {
			goLogin = true;
		}

		if (goLogin) {
			Store.logout()
			history.push("/login")
		}

		let url = "http://localhost:2400/clientes/get";
		let settings = {
			"method" : "GET",
			"headers" : {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + token
			}
		}

		const fetchResponse = await fetch(url, settings).catch((error) => {
			return new Response(
				JSON.stringify({
					error: "Error en la conexión con el servidor"
				}),
				{ "status" : 400}
			);
		});
		let dataGet;
		try{
			dataGet = await fetchResponse.json();
		}
		catch(e) {
			dataGet = {"error": "Error al realizar la petición"};
		}
		let new_data = null;
		if (dataGet && dataGet["success"] === true) {
			new_data = dataGet["data"];
		}
		else{
			message.error(dataGet["error"]);
		}
		if(token) {
			setData(new_data);
		}
		else{
			setData(null);
		}
	}

	useEffect(() => {
		getClientesData();
	}, []);


	const openDelete =  (id) => {
		confirm({
			title: '¿Realmente deseas eliminar este cliente?',
			//icon: <ExclamationCircleOutlined />,
			content: 'Se eliminaran todos sus datos definitivamente',
			async onOk() {
				let url = "http://localhost:2400/cliente/delete/" + id;
				console.log(url);
				let settings = {
					"method" : "DELETE",
					"headers" : {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
					}
				}

				const fetchResponse = await fetch(url, settings).catch((error) => {
					return new Response(
						JSON.stringify({
							error: "Error en la conexión con el servidor"
						}),
						{ "status" : 400}
					);
				});
				let dataGet;
				try{
					dataGet = await fetchResponse.json();
				}
				catch(e) {
					dataGet = {"error": "Error al realizar la petición"};
				}
				let new_data = null;
				if (dataGet && dataGet["success"] === true) {
					getClientesData();
				}
				else{
					message.error(dataGet["error"]);
				}
			},
			onCancel() {
			},
		});
	}

	const openUpdate = (data) => {
		openFormEdit(data)
	}

	const openAdd = () => {
		openFormEdit()
	}


	if (!data) {
		return (
			<div>
				<Header />
				<Skeleton />
			</div>
		);
	}

	const columns = [
		{
			title: 'Nombres',
			dataIndex: 'nombres',
			key: 'nombres',
		},
		{
			title: 'Apellidos',
			dataIndex: 'apellidos',
			key: 'apellidos',
		},
		{
			title: 'Email',
			dataIndex: 'email',
			key: 'email',
		},
		{
			title: 'Cédula',
			dataIndex: 'cedula',
			key: 'cedula',
		},
	];

	if (admin) {
		columns.push(
			{
				title: 'Operaciones',
				dataIndex: 'id',
				key: 'operaciones',
				render: (id, data) => (
						<>
							<Tooltip title="Editar usuario">
								<Button icon={<EditFilled />} onClick={(e) => openUpdate(data)} type="primary" shape="circle"></Button>
							</Tooltip>
							<Tooltip title="Eliminar usuario">
								<Button icon={<DeleteFilled />} onClick={(e) => openDelete(data["id"])} danger shape="circle"></Button>
							</Tooltip>
						</>
					),
			},
		)
	}

	return (
		<div>
			<Header label={admin ? "Administrador" : "Invitado"} />
			<div className="user-profile">
				{
					admin ?
						<div>
							<Button type="primary" onClick={openAdd}>Agregar</Button>
							<Divider />
							<ClienteForm updateParent={getClientesData} forwardRef={e => {openFormEdit = e; }}/>
						</div>
					:
					null
				}
				<Table
					columns={columns}
					dataSource={data}
					pagination={{
						onChange: page => {
							console.log(page);
						},
						pageSize: 5,
					}}
					rowKey="id"  />
			</div>
		</div>
	)
}


export default ProfilePage;
