/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página para iniciar sesión
*/

// React
import { useHistory } from "react-router-dom";
import {useEffect} from 'react';

// Own components
import LoginForm     from '../components/LoginForm';
import Store         from '../utils/Store';

// Antd components

function LoginPage() {
	let history = useHistory();

	useEffect(() => {
		if (Store.getID()) {
			history.push("/profile")
		}
	}, [history]);

	return (
		<div className={"login-register"}>
			<div className="login-panel">
				<LoginForm />
			</div>
		</div>
	)
}


export default LoginPage;
