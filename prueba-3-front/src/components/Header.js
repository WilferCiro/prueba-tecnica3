/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página para iniciar sesión
*/

// React
import { useHistory } from "react-router-dom";

// Own components
import Store         from '../utils/Store';

// Antd components
import {
	PageHeader,
	Button
} from 'antd';

function Header({label}) {
	const history = useHistory();

	const cerrarSesion = () => {
		Store.logout();
		history.push("/login");
	}

	return (
		<header>
			<PageHeader
				ghost={false}
				backIcon={null}
				title={"Perfil del usuario: " + label}
				subTitle=""
				extra={[
					<Button key="1" type="primary" onClick={cerrarSesion}>
						Cerrar sesión
					</Button>,
				]}
				/>
		</header>
	)
}


export default Header;
