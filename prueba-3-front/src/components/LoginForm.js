/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página para iniciar sesión
*/

// React
import {useRef, useContext} from 'react';
import { useHistory } from "react-router-dom";

// Own components
//import Store from '../utils/Store';
import {UserContext} from '../context/UserContext';
// Antd components
import { Form, Input, Button, message } from 'antd';


function LoginForm() {
	let history = useHistory();
	const refForm = useRef(null);

	const userContext = useContext(UserContext);

	const onFinish = async (values: any) => {
		let body = {
			"email" : values["email"],
			"password" : values["password"]
		}
		let url = "http://localhost:2400/login";
		let settings = {
			"method" : "POST",
			"headers" : {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			"body": JSON.stringify(body)
		}

		const fetchResponse = await fetch(url, settings).catch((error) => {
			return new Response(
				JSON.stringify({
					error: "Error en la conexión con el servidor"
				}),
				{ "status" : 400}
			);
		});
		let dataGet;
		try{
			dataGet = await fetchResponse.json();
		}
		catch(e) {
			dataGet = {"error": "Error al realizar la petición"};
			message.error("Error, contacte con el soporte.");
		}

		if (dataGet && dataGet["success"] === true) {
			//Store.storeID(dataGet["token"]);
			userContext.user = dataGet["token"];
			history.push("/profile")
			message.success("Inicio de sesión exitoso, redireccionando...");
		}
		else{
			message.error(dataGet["error"]);
		}
	}

	const onFinishFailed = (errorInfo: any) => {
		message.error("Verifique sus datos para continuar");
	};

	return (
		<div className="login-form">
			<h1>Iniciar sesión</h1>
			<Form
				name="form-login"
				labelCol={{ span: 24 }}
				wrapperCol={{ span: 24 }}
				onFinish={onFinish}
				onFinishFailed={onFinishFailed}
				ref={refForm}
			>
				<Form.Item
					label="Correo electrónico"
					name="email"
					rules={[{ required: true, type:"email", message: 'Por favor ingrese su correo electrónico' }]}
				>
					<Input />
				</Form.Item>

				<Form.Item
					label="Contraseña"
					name="password"
					rules={[{ required: true, message: 'Por favor ingrese su contaseña' }]}
				>
					<Input.Password />
				</Form.Item>

				<Form.Item>
					<Button size="large" type="primary" htmlType="submit" block>
						Iniciar sesión
					</Button>
				</Form.Item>
			</Form>

		</div>
	)
}


export default LoginForm;
