/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página para iniciar sesión
*/

// React

// Own components

// Antd components

function Footer() {
	return (
		<footer>
			Creado por Wilfer Daniel Ciro Maya
		</footer>
	)
}


export default Footer;
