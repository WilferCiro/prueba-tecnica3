/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página para iniciar sesión
*/

// React
import {useRef, useState, useContext} from 'react';

// Own components
//import Store from '../utils/Store';
import {UserContext}    from '../context/UserContext';

// Antd components
import { Form, Input, Button, message, Modal } from 'antd';


function ClienteForm({forwardRef, updateParent}) {
	const refForm = useRef(null);
	const userContext = useContext(UserContext);

	const [visible, setVisible] = useState(false);
	const [uData, setUData] = useState(null);

	const open = (data) => {
		if (data) {
			setUData(data);
			//refForm.current.setFieldsValue(data);
		}
		setVisible(true);
	}

	const close = () => {
		setUData(null);
		setVisible(false);
	}

	forwardRef(open);

	const onFinish = async (values: any) => {
		let body = {
			"email" : values["email"],
			"nombres" : values["nombres"],
			"apellidos" : values["apellidos"],
			"cedula" : values["cedula"],
		}
		let url = "";
		let method = "";
		if (uData) {
			url = "http://localhost:2400/cliente/update/" + uData["id"];
			method = "PUT";
		}
		else{
			url = "http://localhost:2400/cliente/add";
			method = "POST";
		}

		//let token = Store.getID();
		let token = userContext.user;

		let settings = {
			"method" : method,
			"headers" : {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + token
			},
			"body": JSON.stringify(body)
		}

		const fetchResponse = await fetch(url, settings).catch((error) => {
			return new Response(
				JSON.stringify({
					error: "Error en la conexión con el servidor"
				}),
				{ "status" : 400}
			);
		});
		let dataGet;
		try{
			dataGet = await fetchResponse.json();
		}
		catch(e) {
			message.error("Error, contacte con el soporte.");
		}

		if (dataGet && dataGet["success"] === true) {
			updateParent();
			setVisible(false);
			message.success("Operación realizada con éxito");
		}
		else{
			message.error(dataGet["error"]);
		}
	}

	const onFinishFailed = (errorInfo: any) => {
		message.error("Verifique sus datos para continuar");
	};

	return (

		<Modal title={"Formulario de cliente"} footer={[]} visible={visible} onCancel={close} destroyOnClose={true}>
			<Form
				name="form-login"
				labelCol={{ span: 24 }}
				wrapperCol={{ span: 24 }}
				onFinish={onFinish}
				onFinishFailed={onFinishFailed}
				ref={refForm}
				initialValues={uData}
			>
				<Form.Item
					label="Nombres"
					name="nombres"
					rules={[{ required: true, message: 'Por favor ingrese los nombres' }]}
				>
					<Input placeholder="Nombres" />
				</Form.Item>
				<Form.Item
					label="Apellidos"
					name="apellidos"
					rules={[{ required: true, message: 'Por favor ingrese los apellidos' }]}
				>
					<Input placeholder="Apellidos" />
				</Form.Item>
				<Form.Item
					label="Cédula"
					name="cedula"
					rules={[{ required: true, message: 'Por favor ingrese la cédula' }]}
				>
					<Input placeholder="Cédula" />
				</Form.Item>
				<Form.Item
					label="Correo electrónico"
					name="email"
					rules={[{ required: true, type:"email", message: 'Por favor ingrese el correo electrónico' }]}
				>
					<Input placeholder="Correo electrónico" />
				</Form.Item>

				<Form.Item>
					<Button size="large" type="primary" htmlType="submit" block>
						{uData ? "Editar usuario" : "Agregar usuario"}
					</Button>
				</Form.Item>
			</Form>

		</Modal>
	)
}


export default ClienteForm;
