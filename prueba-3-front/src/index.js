/*
	Creado por Wilfer Daniel Ciro Maya - 2021, Prueba técnica 

	Página de rutas
*/

// React
import React              from 'react';
import ReactDOM           from 'react-dom';
import reportWebVitals    from './reportWebVitals';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { createBrowserHistory } from "history";

// Own components
import Footer        from './components/Footer';
// Pages
import LoginPage   from "./pages/Login";
import ProfilePage from './pages/Profile';
import {UserContext} from './context/UserContext';

// Styles
import  './css/index.css';
import  './css/responsive.css';
import 'antd/dist/antd.css';


const history = createBrowserHistory();

ReactDOM.render(
	<div>
		<div className="page">
			<UserContext.Provider value={{}}>
				<Router>
					<Switch>
						<Route history={history} path='/login' component={LoginPage} />
						<Route history={history} path='/profile' component={ProfilePage} />
						<Redirect to="/login" />
					</Switch>
				</Router>
			</UserContext.Provider>
		</div>
		<Footer />
	</div>,
	document.getElementById('root')
);

reportWebVitals();
